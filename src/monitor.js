const glob = require('glob-all');
const chokidar = require('chokidar');
const axios = require('axios');
const os = require('os');
const path = require('path');
const fs = require('fs');

const Monitor = function (config) {
    const filesToWatch = glob.sync(config.paths);

    console.log('Watching', JSON.stringify(filesToWatch))

    const watcher = chokidar.watch(filesToWatch, {
        depth: 1,
    })

    const submit = async filePath => {
        const server = os.hostname();
        const name = path.basename(filePath);
        const log = fs.readFileSync(filePath, 'utf8');

        try {
            console.log(`Updating logs for ${server}`)

            const { data } = await axios.post(config.endpoint, {
                server,
                name,
                log,
                path: filePath,
                key: config.key,
            }, {
                headers: {
                    'User-Agent': 'Orinoco Monitor',
                    'Accept': 'application/json',
                }
            })

            console.log(data);
        } catch (error) {
            let _error$response, _error$response$data;
            console.log((error === null || error === void 0 ? void 0 : (_error$response = error.response) === null || _error$response === void 0 ? void 0 : (_error$response$data = _error$response.data) === null || _error$response$data === void 0 ? void 0 : _error$response$data.errors) || error);
        }
    }

    watcher.on('change', filePath => {
        console.log(`${filePath} changed.`)
        submit(filePath);
    })

    this.shutdown = () => {
        return watcher.close();
    }
}

module.exports = Monitor
