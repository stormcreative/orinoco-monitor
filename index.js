const Monitor = require('./src/monitor')
const config = require('./src/config')

let monitor;

const refreshMonitor = async () => {
    if (monitor) {
        console.log('Refreshing monitor')
        await monitor.shutdown()
    }

    monitor = new Monitor(config)
}

const ONE_HOUR = 60 * 60 * 1000;

setTimeout(() => {
    refreshMonitor();
}, ONE_HOUR)

refreshMonitor()
